# Credits go to stackoverflow user user10610334
# SPDX-Identifier: CC-BY-NC-4.0

import unittest
from io import BytesIO as IO
from mqttcalc.rest_calculator import RESTCalculator


class TestableHandler(RESTCalculator):
    # On Python3, in socketserver.StreamRequestHandler, if this is
    # set it will use makefile() to produce the output stream. Otherwise,
    # it will use socketserver._SocketWriter, and we won't be able to get
    # to the data
    wbufsize = 1

    def finish(self):
        # Do not close self.wfile, so we can read its value
        self.wfile.flush()
        self.rfile.close()

    def date_time_string(self, timestamp=None):
        """Mocked date time string"""
        return "DATETIME"

    def version_string(self):
        """mock the server id"""
        return "BaseHTTP/x.x Python/x.x.x"


class MockSocket(object):
    def getsockname(self):
        return ("sockname",)


class MockRequest(object):
    _sock = MockSocket()

    def __init__(self, path, post_data=None):
        self._path = path
        self._post_data = post_data

    def makefile(self, *args, **kwargs):
        if args[0] == "rb":
            if self._post_data:
                return IO(
                    b"POST %s HTTP/1.0\nContent-Length: %d\n\n%s"
                    % (self._path, len(self._post_data), self._post_data)
                )
            else:
                return IO(b"GET %s HTTP/1.0" % self._path)
        elif args[0] == "wb":
            return IO(b"")
        else:
            raise ValueError("Unknown file type to make", args, kwargs)


class HTTPRequestHandlerTestCase(unittest.TestCase):
    def _test(self, request):
        handler = TestableHandler(request, (0, 0), None)
        return handler.wfile.getvalue().decode("utf-8")

    def test_unsupported_get(self):
        self.assertRegex(self._test(MockRequest(b"/")), r"501 Unsupported method")

    def test_calc(self):
        res = self._test(MockRequest(b"/calc", b'{"a": 1, "b": 5}'))
        self.assertRegex(res, "200 OK")
        self.assertRegex(res, '{"result": 6}')


if __name__ == "__main__":
    unittest.main()
