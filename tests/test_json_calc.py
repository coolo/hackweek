#! /usr/bin/python3

import unittest
import mqttcalc
import mqttcalc.json_calculator as json_calculator


class TestJSON(unittest.TestCase):
    def test_invalid_json(self):
        self.assertIsNone(json_calculator.convert_and_add(""))
        self.assertIsNone(json_calculator.convert_and_add("{'hallo'}"))
        # needs double quote - JSON is great
        self.assertIsNone(json_calculator.convert_and_add("{'a': 1, 'b': 2}"))
        self.assertIsNone(json_calculator.convert_and_add('{"a": 1, "b": "hallo"}'))
        # we're strongly typed
        self.assertIsNone(json_calculator.convert_and_add('{"a": 1, "b": "2"}'))
        self.assertIsNone(json_calculator.convert_and_add('{"a": 1, "b": 2.5}'))
        # we expect a and b
        self.assertIsNone(json_calculator.convert_and_add('{"a": 1, "c": 2}'))

    def test_valid_sums(self):
        self.assertEqual(
            json_calculator.convert_and_add('{"a": 1, "b": 2}'), '{"result": 3}'
        )
        self.assertEqual(
            json_calculator.convert_and_add('{"a": 1, "b": -1}'), '{"result": 0}'
        )


if __name__ == "__main__":
    unittest.main()
