#! /usr/bin/python3

# SPDX-Identifier: MIT

import time
import unittest
import os
import signal

import mqttcalc.server as mqs
import mqttcalc.utils as mqu

# TODO: this test relies on a public server, so it's supercalifragilisticexpialidocious
# chances we collide with someone trying the same are slim, but relying on online services
# like this is super nonsense.
# see dist/ci/docker-compose for a start to test this against a local broker within a
# defined environment (unfortunately docker-compose is harder to run on gitlab than it is elsewhere)
class MQTTFullTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.server_pid = None
        self.messages = []
        self.start_server()
        # give the server process some time
        time.sleep(1)
        return super().setUp()

    def tearDown(self) -> None:
        self.stop_server()
        return super().tearDown()

    def assertCalc(self, client, out, expected_in):
        self.messages = []
        mqu.publish(client, mqu.CALC_INCOMING, out)
        for _ in range(0, 5):
            time.sleep(0.2)
            if self.messages:
                self.assertEqual(self.messages, [expected_in])

    def test_full(self):
        def store_message(_client, _userdata, msg):
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
            self.messages.append(msg.payload.decode())

        # prepare to publish
        broker, client_id, username, password = mqu.get_public_emqx()
        client = mqu.connect_mqtt(broker, client_id, username, password)
        client.subscribe(topic=mqu.CALC_OUTGOING)
        client.on_message = store_message
        # starts a thread in the background
        client.loop_start()

        self.assertCalc(client, '{"a": 12, "b": 7}', '{"result": 19}')
        self.assertCalc(client, '{"c": 12, "b": 7}', '{"error": "invalid format"}')

    def start_server(self):
        pid = os.fork()
        if pid > 0:
            self.server_pid = pid
            return
        broker, client_id, username, password = mqu.get_public_emqx()
        try:
            mqs.run(broker, client_id, username, password)
        except KeyboardInterrupt:
            pass
        os._exit(0)

    def stop_server(self):
        if not self.server_pid:
            return
        os.kill(self.server_pid, signal.SIGINT)
        os.wait()
        self.server_pid = None
