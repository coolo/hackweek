# Copyright: Stephan Kulow
# SPDX-Identifier: CC0

import json
from typing import Optional

# takes string which represents a calculator payload as JSON.
# if the format is valid, a valid JSON answer is returned.
# Otherwise None is returned
def convert_and_add(jsons: str) -> Optional[str]:
    try:
        data = json.loads(jsons)
    except json.decoder.JSONDecodeError:
        return None

    if "a" in data and "b" in data:
        return add_as_json(data["a"], data["b"])

    return None


# raise ValueError if not an integer
def check_int(s: str) -> int:
    if type(s) != int:
        raise ValueError("Not an int")
    return s


# if both a and b are valid integers, we return JSON string,
# otherwise None
def add_as_json(astr: str, bstr: str) -> Optional[str]:
    try:
        sum = check_int(astr) + check_int(bstr)
    except ValueError as e:
        return None

    return '{{"result": {sum}}}'.format(sum=sum)
