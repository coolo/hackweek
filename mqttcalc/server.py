#! /usr/bin/python3

from paho.mqtt import client as mqtt_client

import http.server
from threading import Thread

import mqttcalc.rest_calculator as rest_calculator
import mqttcalc.json_calculator as json_calculator
import mqttcalc.utils as mqu

HTTP_PORT = 8082


def publish_sum(client: mqtt_client.Client, _userdata, msg: mqtt_client.MQTTMessage):
    json_ret = json_calculator.convert_and_add(msg.payload.decode())
    if json_ret is not None:
        mqu.publish(client, mqu.CALC_OUTGOING, json_ret)
    else:
        mqu.publish(client, mqu.CALC_OUTGOING, '{"error": "invalid format"}')


def run_httpd_thread():
    Handler = rest_calculator.RESTCalculator
    with http.server.HTTPServer(("", HTTP_PORT), Handler) as httpd:
        print("serving http at port", HTTP_PORT)
        # start the server in a background thread
        try:
            httpd.serve_forever()
        except KeyboardInterrupt as e:
            print("Shutdown http server")
            httpd.shutdown()


def run(broker: str, client_id: str, username: str, password: str):
    client = mqu.connect_mqtt(broker, client_id, username, password)

    # start REST interface
    Thread(target=run_httpd_thread).start()
    # start MQTT interface
    client.subscribe(topic=mqu.CALC_INCOMING)
    client.on_message = publish_sum
    client.loop_forever()
