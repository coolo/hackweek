#! /usr/bin/python3

import sys
import getopt

import mqttcalc.server as mqs
import mqttcalc.utils as mqu


def usage(cmd):
    print(
        f"Usage: {cmd} -b|--broker <BROKER> -c|--client_id <CLIENT_ID> -u|--user <USER> -p|--password <PASSWORD>"
    )


def main() -> int:
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "b:c:u:p:h",
            ["broker=", "client_id=", "user=", "password=", "help"],
        )
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage(sys.argv[0])
        sys.exit(2)
    broker, client_id, username, password = mqu.get_public_emqx()
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-p", "--password"):
            password = a
        elif o in ("-b", "--broker"):
            broker = a
        elif o in ("-c", "--client_id"):
            client_id = a
        elif o in ("-u", "--user"):
            username = a
        else:
            assert False, "unhandled option"
    try:
        # endless function
        mqs.run(broker, client_id, username, password)
    except KeyboardInterrupt:
        pass

    return 0
