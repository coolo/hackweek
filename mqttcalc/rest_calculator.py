# Copyright: Stephan Kulow
# SPDX-Identifier: CC0

import http.server
from http import HTTPStatus
import mqttcalc.json_calculator as json_calculator

# Incapable REST interface - will return 501 to all requests
# not explicitly implemented
class RESTCalculator(http.server.BaseHTTPRequestHandler):

    # if the POST data is valid JSON (no matter what the content type says),
    # reply with the sum. If not, send them a 400
    def handle_calc(self):
        request_headers = self.headers
        content_length = request_headers["content-length"]
        length = int(content_length) if content_length else 0
        json_bytes = self.rfile.read(length)
        json_return = json_calculator.convert_and_add(
            json_bytes.decode(encoding="utf-8")
        )
        if json_return is None:
            print("invalid calc", length)
            # invalid format
            self.send_response(HTTPStatus.BAD_REQUEST)
            return
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(json_return.encode(encoding="utf-8"))

    def do_POST(self):
        if self.path == "/calc":
            self.handle_calc()
            return
        self.send_response(HTTPStatus.NOT_FOUND)
