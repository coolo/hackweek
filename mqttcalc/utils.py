from paho.mqtt import client as mqtt_client
import sys
from typing import Tuple
import random

MQTT_PORT = 1883
CALC_INCOMING = "calculator/add"
CALC_OUTGOING = "calculator/add/result"


def connect_mqtt(broker, client_id, username, password) -> mqtt_client.Client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
            sys.exit(1)

    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, MQTT_PORT)
    return client


def publish(client: mqtt_client.Client, topic: str, msg: str) -> None:
    result = client.publish(topic, msg)
    # result: [0, 1]
    status = result[0]
    if status == 0:
        print(f"Sent `{msg}` to topic `{topic}`")
    else:
        print(f"Failed to send message to topic {topic}")


def get_public_emqx() -> Tuple[str, str, str, str]:
    broker = "broker.emqx.io"
    client_id = f"hackweek-mqtt-{random.randint(0, 1000)}"
    username = "emqx"
    password = "public"

    return broker, client_id, username, password
