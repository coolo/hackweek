# Documentation 

This repo contains a python implementation of a calculator application that serves both a REST
interface and a MQTT interface. 

Both are stateless interfaces. The REST interface serves under port 8082 a /calc route as method POST
while the MQTT interface subscribes to the calculator/add topic on the public broker broker.emqx.io
to then publish there on the calculator/add/result topic

## Manifest
- mqttcalc/json_calculator.py: provides convert_and_add as main entrance function taking string for string
- mqttcalc/rest_calculator.py: provides RESTCalculator handling HTTP Requests (not a server)
- mqttcalc/utils.py:           helper functions for tests and server
- mqttcalc/server.py:          serving HTTP and MQTT - main entrance function: run
- mqttcalc/commandline.py:     providing command line interface to start the server
- tests/*.py:                  unittest files
- dist/Dockerfile:             distribution container

## Build the distribution
1. First build the python whl with `python3 -m build`. This will output the whl in dist
1. docker build -t hackweek_coolo dist
1. Test with docker run -ti hackweek_coolo

## Goal

Develop in typescript calculator application, which is adding two integers coming as inputs.

### Acceptance Criterias

* The application should offer simple REST API interface taking inputs for add operation and returning calculation result
* The application should offer Mqtt interface
* Subscribe for topic e.g. calculator/add and accept payload e.g. {“a”:10, “b”:20}
* Publish result calculator/add/result e.g. { “result”:30}
* The application’s code should be hosted in a Gitlab repository.
* Document the content of repository so that onboarding of new developers does not need assistance from team
* Document the developed application so that developers from neighbor team can develop next application which could use the calculators interfaces
* Setup pipeline which
  - Will compile and package the calculator application as npm package
  - Will apply audit of external packages and fail on issues only on merge request
  - Will execute tests for calculator application, collect code coverage and make it available as result of the pipeline
* Create Dockerfile which defines docker image hosting calculator application ready to be deployed

### Added

Replacing typescript with python is ok too
